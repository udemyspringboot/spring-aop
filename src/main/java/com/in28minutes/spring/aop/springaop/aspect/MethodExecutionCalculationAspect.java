package com.in28minutes.spring.aop.springaop.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;

// AOP
// Configuration
@Aspect
@Configuration
public class MethodExecutionCalculationAspect {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Around("com.in28minutes.spring.aop.springaop.aspect.CommonJoinPointConfig.trackTimeAnnotation()")
	public void afterReturning(final ProceedingJoinPoint joinPoint) throws Throwable {
		final long startTime = System.currentTimeMillis();
		joinPoint.proceed();
		final long timeTaken = System.currentTimeMillis() - startTime;
		// startTime = x
		// allow execution of method
		// endTime = y
		this.logger.info("Time Taken {} is {}", joinPoint, timeTaken);
	}
}
